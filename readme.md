# CORE21

CORE21 is a virtual vector & microcontroller processor core simulator.

## Design

Software defined virtual machines have many examples. Java had a virtual machine compiled to hardware. Intel made its own model using the amd64 instruction set.

Such software defined virtual processors have many advantages. Abstraction allows scaling. Portability helps to use the same software on different architectures.

Core21 is a Go implementation of the most simple machine model possible. This makes it a good educational tool that translates directly to hardware. Still it is ready to be used by software professionals for custom instruction sets and vector processing logic.

## Modules

1. The most notable feature is a single area of address space.
2. The address space can be addressed by multiple load and store registers.
3. The best in the design is that everything including the register space is memory mapped.
4. Each core codenamed Core21 or C21 has a shuffle logic to select registers as an input.
5. Shuffle logic can map any bit to any other bit in the input. Shuffle allows a simple logic in the next step.
6. The next layer is a programmable logic set like FPGAs or PLCs. It narrows logic to neighbor blocks so that it can be compressed well.
7. Each logic bit is a binary combination of a bit, and its left and right neighbors.
7. There is another optional shuffle layer to direct bits to an output register. Shuffle allows a simple logic in the previous step.
8. The output register is controlled by store registers to direct back to memory.

## Vector role - Single Instruction Multiple Data

When the core acts as a vector processor, then the logic applies to the same blocks of unified neighbor data.

It can add for example two vectors bit by bit with carry.

Every output register is a logic function of a few input bits.

The last store instruction may shift.

## Processor role - Multiple Instructions Single Data

This approach reserves the first few bits of the vector as the accumulator. The entire vector acts as the context of a regular processor.

Only part of the logic is activated by the instruction used. One part can do addition, the other one forking as a result.

This approach still allows the use of the rest of the logic gates for limited result and branch prediction.

## Optimization

Optimization can be handled by the hardware design. It is not locked down by this specification. The advantage of this unified approach is that the implementation remains portable.

## License

Licensed under Creative Commons CC0.

To the extent possible under law, the author(s) have dedicated all copyright and related and
neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
You should have received a copy of the CC0 Public Domain Dedication along with this software.
If not, see <https:#creativecommons.org/publicdomain/zero/1.0/legalcode>.
