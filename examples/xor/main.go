package main

import (
	"fietspad/core21"
	"fmt"
)

// Licensed under Creative Commons CC0.
//
// To the extent possible under law, the author(s) have dedicated all copyright and related and
// neighboring rights to this software to the public domain worldwide.
// This software is distributed without any warranty.
// You should have received a copy of the CC0 Public Domain Dedication along with this software.
// If not, see <https:#creativecommons.org/publicdomain/zero/1.0/legalcode>.

func main() {
	// xor each value of the register with the previous one.
	var w = 10
	var epochs = 10
	var register = make([]core21.Bit, w)
	for i := 0; i < w; i++ {
		if i%2 == 0 {
			register[i] = 1
		} else {
			register[i] = 0
		}
	}

	var logicXor = make([][]core21.Bit, w)
	for i := 0; i < w; i++ {
		logicXor[i] = []core21.Bit{0, 0, 1, 1, 0, 0, 1, 1}
	}

	core := core21.Core{W: 10, ShuffleA: core21.NoShuffle(w), Logic: logicXor, ShuffleB: core21.NoShuffle(w), Register: register}

	for epoch := 0; epoch < epochs; epoch++ {
		register = core21.Epoch(&core)
		fmt.Println(register[0:10])
	}
}
