package core21

//Licensed under Creative Commons CC0.
//
//To the extent possible under law, the author(s) have dedicated all copyright and related and
//neighboring rights to this software to the public domain worldwide.
//This software is distributed without any warranty.
//You should have received a copy of the CC0 Public Domain Dedication along with this software.
//If not, see <https:#creativecommons.org/publicdomain/zero/1.0/legalcode>.

type Bit byte
type Core struct {
	W        int
	Register []Bit
	ShuffleA []int
	Logic    [][]Bit
	ShuffleB []int
}

func Epoch(core *Core) []Bit {
	var load = make([]Bit, core.W)
	for i := 0; i < core.W; i++ {
		load[i] = core.Register[core.ShuffleA[i]]
	}
	var result = make([]Bit, core.W)
	for i := 0; i < core.W; i++ {
		// Logic can be big for large registers, so we limit it having an input shuffle
		a := ((i - 1) + core.W) % core.W
		b := ((i) + core.W) % core.W
		c := ((i + 1) + core.W) % core.W
		ix := int((load[a] << 2) | (load[b] << 1) | load[c])
		n := len(core.Logic[i]) - 1
		result[i] = core.Logic[i][n-ix]
	}
	var store = make([]Bit, core.W)
	for i := 0; i < core.W; i++ {
		store[i] = result[core.ShuffleB[i]]
	}
	core.Register = store
	return core.Register
}

func NoShuffle(w int) []int {
	var noShuffle = make([]int, w)
	for i := 0; i < w; i++ {
		noShuffle[i] = i
	}
	return noShuffle
}
